//
//  PDetails2ViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 08/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDetails2ViewController : UIViewController



@property(strong,nonatomic)IBOutlet UILabel *lbl_pdate;

@property(strong,nonatomic)IBOutlet UILabel *lbl_wPeriod;
@property(strong,nonatomic)IBOutlet  UILabel *lbl_wExpDate;


-(IBAction)didTapBack2PDetails:(id)sender;



@end
