//
//  FilterViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 31/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ViewController.h"

@interface FilterViewController : ViewController
@property (weak, nonatomic) IBOutlet UIView *Vwcategory;


- (IBAction)didTapcategory:(id)sender;
- (IBAction)didTapBrand:(id)sender;
- (IBAction)didTapSortBy:(id)sender;

- (IBAction)didTapSearch:(id)sender;



- (IBAction)didTapBack:(id)sender;
@end
