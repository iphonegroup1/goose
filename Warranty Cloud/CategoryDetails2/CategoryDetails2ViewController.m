//
//  CategoryDetails2ViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 28/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import "CategoryDetails2ViewController.h"
#import "Cat_Details2TableViewCell.h"
#import "CustomTabbarViewController.h"


@interface CategoryDetails2ViewController ()

@end

@implementation CategoryDetails2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    
    
    
    
    // to call tab bar controller to select 1st item on Tabbar
    
    [CustomTabbarViewController load];
    
    
    [self.tabBarController setSelectedIndex:0];
    
    
    
    _arrtbl_category=[[NSMutableArray alloc]initWithObjects:@"PRD_Item01.png",@"PRD_Item02.png",@"PRD_Item03.png",@"PRD_Item04.png", nil];
    
    
    _arrlblHeading_cat=[[NSMutableArray alloc]initWithObjects:@"Mi Pad Tablet 16 GB",@"Philips DVD Player",@"Sony Laptop",@"Lenovo Tab 2 A7",nil];
    
    
    
    _arrlblDesc_cat=[[NSMutableArray alloc]initWithObjects:@"Introducing the all-new Mi Pad,that comes loaded with best-in-class",@"Your dream player with flawless,picture quality and the ability to play virtually",@"Touchpad with Windows 8.1,multitouch gesture support",@"If you are looking for budget friendly top of the line tablet", nil];
    
    
    
    
    _arrlblTimeLeft_cat=[[NSMutableArray alloc]initWithObjects:@"2 Weeks",@"3 Weeks",@"6 Weeks",@"2 Weeks", nil];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return _arrtbl_category.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cellCategoryDetails";
    
    Cat_Details2TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[Cat_Details2TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    cell.contentView.backgroundColor = [UIColor clearColor];
    // cell.image=[UIImage imageNamed:[_arrtblImages objectAtIndex:indexPath.row]];
    
    cell.img_Category.image=[UIImage imageNamed:[_arrtbl_category objectAtIndex:indexPath.row]];
    cell.lblheading.text=[NSString stringWithFormat:@"%@",[_arrlblHeading_cat objectAtIndex:indexPath.row]];
    
    cell.lblDesc.text=[NSString stringWithFormat:@"%@",[_arrlblDesc_cat objectAtIndex:indexPath.row]];
    
    
    
    
    
    
    cell.lblTimeleft.text=[NSString stringWithFormat:@"%@",[_arrlblTimeLeft_cat objectAtIndex:indexPath.row]];
    
        return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0f;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
}


-(IBAction)didTapBack2_filterCatDetails:(id)sender
{
   
    [self.navigationController popViewControllerAnimated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
