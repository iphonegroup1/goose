//
//  UYSTableViewCell.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 29/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UYSTableViewCell : UITableViewCell



@property(weak,nonatomic)IBOutlet UILabel *lblUYS_space;
@property(weak,nonatomic)IBOutlet UILabel *lblUYS_cost;

@end
