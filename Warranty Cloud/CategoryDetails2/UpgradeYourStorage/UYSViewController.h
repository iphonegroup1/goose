//
//  UYSViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 29/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UYSViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>



@property (weak, nonatomic) IBOutlet UITableView *tblList_UYS;



@property (strong, nonatomic)NSMutableArray *arrlblUYS_space;

@property(strong,nonatomic)NSMutableArray *arrlblUYS_cost;

-(IBAction)didTapBack_Settings:(id)sender;





@end
