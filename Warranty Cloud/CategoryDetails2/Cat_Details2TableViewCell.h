//
//  Cat_Details2TableViewCell.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 28/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cat_Details2TableViewCell : UITableViewCell



@property(weak,nonatomic)IBOutlet UIImageView *img_Category;
@property(weak,nonatomic)IBOutlet UILabel *lblheading;
@property(weak,nonatomic)IBOutlet UILabel *lblDesc;
@property(weak,nonatomic)IBOutlet UILabel *lblTimeleft;
@end
