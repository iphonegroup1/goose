//
//  CategoryDetails2ViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 28/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetails2ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UISearchBar *searchCategory_details;
@property (weak, nonatomic) IBOutlet UITableView *tblList_category;



@property (strong, nonatomic)NSMutableArray *arrtbl_category;

@property(strong,nonatomic)NSMutableArray *arrlblHeading_cat;
@property(strong,nonatomic)NSMutableArray *arrlblDesc_cat;
@property(strong,nonatomic)NSMutableArray *arrlblTimeLeft_cat;




-(IBAction)didTapBack2_filterCatDetails:(id)sender;

@end
