//
//  TabbarCategoryViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 18/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTabbarViewController : UITabBarController<UITabBarControllerDelegate>


@property (strong, nonatomic) IBOutlet UIView *customTabUIView;
@property (weak, nonatomic) IBOutlet UIScrollView *tabUIScrollView;



@property (weak, nonatomic) IBOutlet UIButton *firstUIButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *secondUIButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *thirdUIButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *fourthUIButtonOutlet;




@property(weak,nonatomic)IBOutlet UILabel *lblhome;
@property(weak,nonatomic)IBOutlet UILabel *lblfilter;
@property(weak,nonatomic)IBOutlet UILabel *lblsettings;
@property(weak,nonatomic)IBOutlet UILabel *lblAddnew;


@property(weak,nonatomic)IBOutlet UIImageView *imgbg;
- (IBAction)didTapBarbuttonItems:(UIButton *)sender;


@end
