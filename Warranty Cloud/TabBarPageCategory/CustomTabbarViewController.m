//
//  TabbarCategoryViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 18/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "CustomTabbarViewController.h"
#import "TabBarSearch1TableViewCell.h"
#import "FilterViewController.h"
#import "AddNewViewController.h"
#import "SettingsViewController.h"



@interface CustomTabbarViewController ()
{
    UIScrollView *taabscroll;
    
    NSMutableArray *tabbarImg;
}

@end

@implementation CustomTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
       [self UnSelectAllTheTabs];
   
    [[NSBundle mainBundle] loadNibNamed:@"CustomTabBar" owner:self options:nil];
    //_customTabUIView.frame=CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width, 70);
//    _customTabUIView.frame=CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width, 80);
    
    NSLog(@"image width%f",_imgbg.frame.size.width);
    NSLog(@"image height%f",_imgbg.frame.size.height);
    

    
    _customTabUIView.frame=CGRectMake(0, self.view.frame.size.height-70, self.view.frame.size.width, 70);
    
    
    
    
    [self.view addSubview:_customTabUIView];
    
    [self UnSelectAllTheTabs];// to change the tabbar default HOME button to be selected while loading.
     _lblhome.textColor=[UIColor orangeColor];
    
    
    
   // _customTabUIView.backgroundColor=[UIColor blackColor];
    
    //_tabUIScrollView.contentSize=CGSizeMake(_firstUIButtonOutlet.frame.size.width*4, 0);
    
#pragma new code
//    
// tabbarImg
//    =[[NSMutableArray alloc] initWithObjects:@"home_sc.png",@"Filter_sc-1.png",@"Settings_non_sc.png",@"Add-New_sc.png", nil];
//    self.tabBar.hidden=YES;
//    taabscroll=[[UIScrollView alloc] initWithFrame:self.tabBar.frame];
//    taabscroll.backgroundColor=[UIColor blueColor];
//   [self.view addSubview:taabscroll];
//    [self createbutton];
//    
    


    // Do any additional setup after loading the view.
}


#pragma mark - Custom Tab Buttons


#pragma mark - Unselect All Tab Button

-(void)UnSelectAllTheTabs
{
    
    
     _lblhome.textColor=[UIColor orangeColor];
    UIImage *buttonImage = [UIImage imageNamed:@"home_sc.png"];
    [_firstUIButtonOutlet setBackgroundImage:buttonImage forState:UIControlStateNormal];

    
   // _secondUIButtonOutlet.backgroundColor=[UIColor lightGrayColor];
    //_thirdUIButtonOutlet.backgroundColor=[UIColor lightGrayColor];
   // _fourthUIButtonOutlet.backgroundColor=[UIColor lightGrayColor];
    
    
    //_fifthUIButtonOutlet.backgroundColor=[UIColor lightGrayColor];
    //_sixthUIButtonOutlet.backgroundColor=[UIColor lightGrayColor];
}
#pragma custom a tab selection
/*-(void)activeTabSelect:(int)tag
{
    [self setSelectedViewController:[self.viewControllers objectAtIndex:tag]];
    for (UIButton *btn in taabscroll.subviews)
    {
        if([btn isKindOfClass:[UIButton class]])
        {
            switch (tag)
            {  case 0:
                   // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover.png"]];
                    break;
                    
                case 1:
                   // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover.png"]];
                    break;
                case 2:
                  //  btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover.png"]];
                    break;
                case 3:
                  //  btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover.png"]];
                    break;
               
                default:
                    break;
            }
        }}
    [self deselectOtherButton:tag];
}
*/

#pragma deselect button
/*-(void)deselectOtherButton:(int )tager
{
    for (UIView *v in taabscroll.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)v;
            if(v.tag==tager) continue;
            switch (v.tag)
            {
                case 0:
                   // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover1.png"]];
                    break;
                case 1:
                  //  btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover1.png"]];
                    break;
                case 2:
                   // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover1.png"]];
                    break;
                case 3:
                   // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover1.png"]];
                    break;
                case 4:
                   // btn.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"tab_hover1.png"]];
                    break;
                default:
                    break;
            }}}
}*/
# pragma Switch Case for selecting Tab bar item.

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if(item.tag==1)
    {
        //your code
        
        _lblhome.textColor=[UIColor orangeColor];
    }
    else
    {
        //your code
    }
}


- (IBAction)didTapBarbuttonItems:(UIButton *)sender
{
    [self UnSelectAllTheTabs];
    switch (sender.tag)
    
    {
        case 0:
        {
            
            
            
            _lblhome.textColor=[UIColor orangeColor];
            self.selectedIndex=0;
            
            [self selectedViewController];
            
            
            
            
            
            UIImage *buttonImage = [UIImage imageNamed:@"home_sc.png"];
            [_firstUIButtonOutlet setBackgroundImage:buttonImage forState:UIControlStateNormal];

          
            UIImage *buttonImage1 = [UIImage imageNamed:@"Filter_nc.png"];
           [_secondUIButtonOutlet setBackgroundImage:buttonImage1 forState:UIControlStateNormal];
            
           UIImage *buttonImage2 = [UIImage imageNamed:@"Settings_nc.png"];
            [_thirdUIButtonOutlet setBackgroundImage:buttonImage2 forState:UIControlStateNormal];
            
            UIImage *buttonImage3 = [UIImage imageNamed:@"Add-New_nc.png"];
            [_fourthUIButtonOutlet setBackgroundImage:buttonImage3 forState:UIControlStateNormal];
            
            
            
            _lblfilter.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblsettings.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblAddnew.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
        
                break;
            
        }
        case 1:
        {
            
            _lblfilter.textColor=[UIColor orangeColor];
            
            UIImage *buttonImage = [UIImage imageNamed:@"Filter_sc-1.png"];
           [_secondUIButtonOutlet setBackgroundImage:buttonImage forState:UIControlStateNormal];
           
            self.selectedIndex=1;
            
            
            
            
            UIImage *buttonImage1 = [UIImage imageNamed:@"home_nc.png"];
            [_firstUIButtonOutlet setBackgroundImage:buttonImage1 forState:UIControlStateNormal];
            
            UIImage *buttonImage2 = [UIImage imageNamed:@"Settings_nc.png"];
            [_thirdUIButtonOutlet setBackgroundImage:buttonImage2 forState:UIControlStateNormal];
            
            UIImage *buttonImage3 = [UIImage imageNamed:@"Add-New_nc.png"];
            [_fourthUIButtonOutlet setBackgroundImage:buttonImage3 forState:UIControlStateNormal];
            
            
            
            
            _lblhome.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblsettings.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblAddnew.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
     
//            for (UIView *v in _customTabUIView.subviews)
//            {
//                if([v isKindOfClass:[UIButton class]])
//                {
//                    UIButton *btn = (UIButton *)v;
//                    if(sender.tag==1)
//                        continue;
//                    switch (v.tag)
//                    {
//                        case 0:
//                            [self.navigationController popViewControllerAnimated:YES];
//                            break;
//                        
//                    }
//                }
//            }
            
            
            //[self.navigationController popViewControllerAnimated:YES];

            break;
        }
        case 2:
        {
            
            
            
            
            _lblsettings.textColor=[UIColor orangeColor];
            
            
           UIImage *buttonImage = [UIImage imageNamed:@"Settings_sc-1.png"];
          [ _thirdUIButtonOutlet setBackgroundImage:buttonImage forState:UIControlStateNormal];
          //  _thirdUIButtonOutlet.backgroundColor=[UIColor grayColor];
            
            
            self.selectedIndex=2;
          
            UIImage *buttonImage1 = [UIImage imageNamed:@"Filter_nc.png"];
            [_secondUIButtonOutlet setBackgroundImage:buttonImage1 forState:UIControlStateNormal];
            

            UIImage *buttonImage2 = [UIImage imageNamed:@"home_nc.png"];
            [_firstUIButtonOutlet setBackgroundImage:buttonImage2 forState:UIControlStateNormal];
            
            
            UIImage *buttonImage3 = [UIImage imageNamed:@"Add-New_nc.png"];
            [_fourthUIButtonOutlet setBackgroundImage:buttonImage3 forState:UIControlStateNormal];
            
            
            _lblhome.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblfilter.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblAddnew.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            break;
        }
        case 3:
        {
           
            
            
            _lblAddnew.textColor=[UIColor orangeColor];
            
            
            
          UIImage *buttonImage = [UIImage imageNamed:@"Add-New_sc.png"];
          [_fourthUIButtonOutlet setBackgroundImage:buttonImage forState:UIControlStateNormal];
            
          //  _fourthUIButtonOutlet.backgroundColor=[UIColor grayColor];
            self.selectedIndex=3;
            
            
            
            UIImage *buttonImage1 = [UIImage imageNamed:@"Filter_nc.png"];
            [_secondUIButtonOutlet setBackgroundImage:buttonImage1 forState:UIControlStateNormal];
            
            UIImage *buttonImage2 = [UIImage imageNamed:@"Settings_nc.png"];
            [_thirdUIButtonOutlet setBackgroundImage:buttonImage2 forState:UIControlStateNormal];
            
            UIImage *buttonImage3 = [UIImage imageNamed:@"home_nc.png"];
            [_firstUIButtonOutlet setBackgroundImage:buttonImage3 forState:UIControlStateNormal];
            
            _lblhome.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblsettings.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            _lblfilter.textColor=[UIColor colorWithRed:196.0/255.0f green:195.0/255.0f blue:195.0/255.0f alpha:1.0f];
            
            
            break;
        }
            
        default:
        break;
    }
    
    
}
@end
