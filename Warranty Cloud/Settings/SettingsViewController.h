//
//  SettingsViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 31/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ViewController.h"

@interface SettingsViewController : ViewController


@property (weak, nonatomic) IBOutlet UIView *VwEMailAdd;

@property (weak, nonatomic) IBOutlet UILabel *lblEmailAddress;
- (IBAction)didTapEmailAdd:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblPassword;



- (IBAction)didTapBack:(id)sender;
- (IBAction)didTapPassword:(id)sender;


#pragma Logout 

-(IBAction)didTapLogout:(id)sender;
-(IBAction)didTapUpgrade_storage:(id)sender;



@end
