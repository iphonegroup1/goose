//
//  SettingsViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 31/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "SettingsViewController.h"
#import "ViewController.h"
#import "UYSViewController.h"

#import "CustomTabbarViewController.h"


@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



#pragma Logout




-(IBAction)didTapLogout:(id)sender
{
    
    //[self.tabBarController presentViewController:ViewController animated:YES];
    
       // [self performSegueWithIdentifier:@"back2Login" sender:self];
    
    [self doExit];
    
    
    
}

-(IBAction)doExit
{
    //show confirmation message to user
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                                    message:@"Are you sure you want to quit GOOSE?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)  // 0 == the cancel button
    {
        //home button press programmatically
        UIApplication *app = [UIApplication sharedApplication];
        [app performSelector:@selector(suspend)];
        
        //wait 2 seconds while app is going background
        [NSThread sleepForTimeInterval:2.0];
        
        
        //exit app when app is in background
        exit(0);
    }
}




-(IBAction)didTapUpgrade_storage:(id)sender
{
    
    //[self performSegueWithIdentifier:@"upgrade2Storage" sender:self];
    
    //[self.navigationController pushViewController:UYSViewController* animated:YES];
    
#pragma To dismisss tab bar
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    UYSViewController *uys = [self.storyboard instantiateViewControllerWithIdentifier:@"UYStorage"];
    uys.hidesBottomBarWhenPushed=YES;
//    [self.navigationController pushViewController:uys animated:YES];
    [self.navigationController presentViewController:uys animated:NO completion:nil];
    
    
   // uys.hidesBottomBarWhenPushed=YES;
//didTapBarbuttonItems:(UIButton *)sender
  
    

    //CustomTabbarViewController *ctvc=[[CustomTabbarViewController alloc]init];
    

    
    //switch (sender.tag)
        
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)didTapBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)didTapEmailAdd:(id)sender {
}
- (IBAction)didTapPassword:(id)sender {
}

-(void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"This is the disappear method being called::::::");
    [self.navigationController.tabBarController setHidesBottomBarWhenPushed:YES
     ];
}




@end
