//
//  ViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 08/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LoginViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
  
   /*
   _imgVwAddfirstItem.layer.backgroundColor=[[UIColor clearColor] CGColor];
    
    _imgVwAddfirstItem.layer.cornerRadius=2.0f;
    
    _imgVwAddfirstItem.layer.borderWidth=2.0;
    _imgVwAddfirstItem.layer.masksToBounds = YES;
    _imgVwAddfirstItem.layer.borderColor = [UIColor colorWithRed:112/255. green:134/255. blue:165/255. alpha:1.0].CGColor;
    
    */

    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    
    
        [textField resignFirstResponder];
    return YES;
    



}




-(void) showMessage:(NSString*)message withTitle:(NSString *)title
{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"Ok"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                [self performSegueWithIdentifier:@"Login" sender:self];
                                 
                             }];
    
    
    UIAlertAction *resetAction = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Cancel", @"cancel")
                                  style:UIAlertActionStyleDestructive
                                  handler:^(UIAlertAction *action)
                                  {
                                      NSLog(@"cancel");
                                  }];
    
    UIAlertAction* skip = [UIAlertAction
                         actionWithTitle:@"Skip for Now"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self performSegueWithIdentifier:@"Login" sender:self];
                             
                         }];
    
    

    [alert addAction:ok];
    [alert addAction:resetAction];
    [alert addAction:skip];
 
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma To login with Touch Id
- (IBAction)didTapLogin:(id)sender {
    
   
    [self performSegueWithIdentifier:@"Login" sender:self];
    
    
    
    
    
    //LAContext *myContext = [[LAContext alloc] init];
    //NSError *authError =nil;
   // myContext.localizedFallbackTitle = @"GOOSE";
   
    
   // NSString *myLocalizedReasonString = @"Please authenticate to proceed";
    
    
   // NSString *skip=@"Skip for Now";
    //[self showMessage:@"Skip for Now" withTitle:@"Skip"];
    /*
     if ([myContext canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&authError])
    {
          [myContext evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                  localizedReason:myLocalizedReasonString
                      reply:^(BOOL succes, NSError *error)
           
                     //reply:^(BOOL succes, NSError *error)
           
        {
                                
                                if (succes) {
                                    
                            //[self showMessage:@"Authentication is successful" withTitle:@"GOOSE"];
                                    NSLog(@"User authenticated");
                   //LoginViewController  *loginView = [self.storyboard instantiateViewControllerWithIdentifier:@"Login"];
                                  
               // [self.navigationController pushViewController:loginView animated:YES];
                                   // [self.navigationController pushViewController: animated:YES];
                        
                         [self showMessage:@"Skip for Now" withTitle:@"Skip"];
                                    
                      //  [self performSegueWithIdentifier:@"Login" sender:self];
   
                                
                                }
                                
                                
                                
                    else {
                        
                        
                                    switch (error.code) {
                                        case LAErrorAuthenticationFailed:
                                           // [self showMessage:@"Authentication is failed" withTitle:@"Error"];
                                            NSLog(@"Authentication Failed");
                                            break;
                                            
                                        case LAErrorUserCancel:
                                           // [self showMessage:@"You clicked on Cancel" withTitle:@"Error"];
                                           // NSLog(@"User pressed Cancel button");
                                            
                                           // [self performSegueWithIdentifier:@"login" sender:nil];
                                           
                                            [self performSegueWithIdentifier:@"Login" sender:self];

                                            break;
                                            
                                        case LAErrorUserFallback:
                                            [self showMessage:@"You clicked on \"Enter Password\"" withTitle:@"Enter Password"];
                                            
                                            NSLog(@"User pressed \"Enter Password\"");
                                            break;
                                            
                                        default:
                                            [self showMessage:@"Touch ID is not configured" withTitle:@"Error"];
                                            NSLog(@"Touch ID is not configured");
                                            break;
                                    }
                                    
                                    NSLog(@"Authentication Fails");
                                    
                                }
                            }];
        
    }*/
   // else
    /*{
        NSLog(@"Can not evaluate Touch ID");
        [self showMessage:@"Authentication is successful" withTitle:@"GOOSE"];
        
        //[self showMessage:@"Can not evaluate TouchID" withTitle:@"Error"];
        
       // [self performSegueWithIdentifier:@"login" sender:nil];
        
        
        
    }*/

    
}

-(IBAction)didTapAddFirstItem:(id)sender
{
    

    
    
    [self performSegueWithIdentifier:@"Add1stitemToReceiptPhoto1" sender:nil];
    
    
    
    
    
    
    
    
}

@end
