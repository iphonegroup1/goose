//
//  TabbarSearch1ViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 18/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "TabbarSearch1ViewController.h"
#import "TabBarSearch1TableViewCell.h"
#import "CustomTabbarViewController.h"




@interface TabbarSearch1ViewController ()

@end

@implementation TabbarSearch1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    // to call tab bar controller to select 1st item on Tabbar
    
    [CustomTabbarViewController load];
    
    
    [self.tabBarController setSelectedIndex:0];
    
    

    _arrtblImages=[[NSMutableArray alloc]initWithObjects:@"PRD_Item01.png",@"PRD_Item02.png",@"PRD_Item03.png",@"PRD_Item04.png", nil];
    
    
    _arrlblHeading=[[NSMutableArray alloc]initWithObjects:@"Mi Pad Tablet 16 GB",@"Philips DVD Player",@"Sony Laptop",@"Lenovo Tab 2 A7",nil];
    
                 
                 
    _arrlblDesc=[[NSMutableArray alloc]initWithObjects:@"Introducing the all-new Mi Pad,that comes loaded with best-in-class",@"Your dream player with flawless,picture quality and the ability to play virtually",@"Touchpad with Windows 8.1,multitouch gesture support",@"If you are looking for budget friendly top of the line tablet", nil];
    
    
    
    
    _arrlblTimeLeft=[[NSMutableArray alloc]initWithObjects:@"2 Weeks",@"3 Weeks",@"6 Weeks",@"2 Weeks", nil];
    
    
//     _lblhome.textColor=[UIColor orangeColor];
//    UIImage *buttonImage = [UIImage imageNamed:@"home_sc.png"];
    
//    [_firstUIButtonOutlet setBackgroundImage:buttonImage forState:UIControlStateNormal];
//
    
    
    
    // Do any additional setup after loading the view.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
       return _arrtblImages.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"cellHome";
    
    TabBarSearch1TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        cell = [[TabBarSearch1TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.selectedBackgroundView=UITableViewCellSelectionStyleNone;
    

    
   // cell.image=[UIImage imageNamed:[_arrtblImages objectAtIndex:indexPath.row]];
    
    cell.imgPro.image=[UIImage imageNamed:[_arrtblImages objectAtIndex:indexPath.row]];
    cell.lblheading.text=[NSString stringWithFormat:@"%@",[_arrlblHeading objectAtIndex:indexPath.row]];
    
    cell.lblDesc.text=[NSString stringWithFormat:@"%@",[_arrlblDesc objectAtIndex:indexPath.row]];
    
    

    cell.lblTimeleft.text=[NSString stringWithFormat:@"%@",[_arrlblTimeLeft objectAtIndex:indexPath.row]];
    
    
    
    
   
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 90.0f;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
//    UIAlertView *messageAlert = [[UIAlertView alloc]
//                                 initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    
//    // Display Alert Message
    //[messageAlert show];
  [self performSegueWithIdentifier:@"push2ProDetails" sender:self];

    
    NSLog(@"%ld", (long)indexPath.row); // you can see selected row number in your console;
}



//- (IBAction)didTapBack2DocSaving:(id)sender
//{
//    [self.navigationController popViewControllerAnimated:YES];
//
//}


-(IBAction)didTapBack:(id)sender
{
   
    
  //  [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/









//- (IBAction)didTapBack2DocSaving:(id)sender
//{
//    
//   
//    [self.navigationController popViewControllerAnimated:YES];
//
//}
@end
