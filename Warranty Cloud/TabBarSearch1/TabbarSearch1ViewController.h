//
//  TabbarSearch1ViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 18/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CustomTabbarViewController.h"


@interface TabbarSearch1ViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>





//@property(strong,nonatomic)IBOutlet UIImageView *imgicon;




@property (weak, nonatomic) IBOutlet UISearchBar *searchCategory;
@property (weak, nonatomic) IBOutlet UITableView *tblList;

@property (strong, nonatomic)NSMutableArray *arrtblImages;

@property(strong,nonatomic)NSMutableArray *arrlblHeading;
@property(strong,nonatomic)NSMutableArray *arrlblDesc;
@property(strong,nonatomic)NSMutableArray *arrlblTimeLeft;





-(IBAction)didTapBack:(id)sender;


//- (IBAction)didTapBack2DocSaving:(id)sender;




@end
