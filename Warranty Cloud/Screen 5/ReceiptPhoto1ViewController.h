//
//  Screen5ViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 10/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface ReceiptPhoto1ViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgVwTopScreen;

- (IBAction)didTapBackBtn:(id)sender;


@property (weak, nonatomic) IBOutlet UIImageView *imgVwLogoicon;
@property (weak, nonatomic) IBOutlet UILabel *lbldoYouWant;
@property (weak, nonatomic) IBOutlet UIView *vWmiddlelayer;
@property (weak, nonatomic) IBOutlet UIImageView *imgLaptop;
@property (weak, nonatomic) IBOutlet UIImageView *imgCamera;
@property (weak, nonatomic) IBOutlet UIImageView *imgRadio;
@property (weak, nonatomic) IBOutlet UILabel *lblYourTablet;

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)didTapYesPlease:(id)sender;

- (IBAction)didTapNoThanks:(id)sender;
@end
