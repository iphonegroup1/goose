//
//  Screen7ViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 10/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Screen7ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgOnTop1;


- (IBAction)didTapBack:(id)sender;

- (IBAction)didTapSaveandTakeAnother:(id)sender;

- (IBAction)didTapSaveandContinue:(id)sender;
- (IBAction)didTapTakePhtotoAgain:(id)sender;

@end
