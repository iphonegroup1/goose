//
//  Screen7ViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 10/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "Screen7ViewController.h"

@interface Screen7ViewController ()

@end

@implementation Screen7ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapSaveandTakeAnother:(id)sender {
    
    
#pragma  mark : Action Sheet
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    UIAlertAction* cameraRoll = [UIAlertAction
                                 actionWithTitle:@"Camera Roll"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                     
                                     [self presentViewController:picker animated:YES completion:NULL];
                                     //[alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];
    UIAlertAction* takeaphoto = [UIAlertAction
                                 actionWithTitle:@"Take a photo"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     
                                     
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                     picker.delegate = self;
                                     picker.allowsEditing = YES;
                                     picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     
                                     [self presentViewController:picker animated:YES completion:NULL];
                                     

                                    // [alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];
    
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alertVC dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    
    [alertVC addAction:cameraRoll];
    [alertVC addAction:takeaphoto];
    [alertVC addAction:cancel];
    
    [alertVC.view setTintColor:[UIColor blackColor]];
    [self presentViewController:alertVC animated:YES completion:nil];

    
     //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapSaveandContinue:(id)sender
{
    [self performSegueWithIdentifier:@"pushToReceiptPhoto2" sender:self];
}

- (IBAction)didTapTakePhtotoAgain:(id)sender {
    
    
    
    
#pragma  mark : Action Sheet
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    UIAlertAction* cameraRoll = [UIAlertAction
                                 actionWithTitle:@"Camera Roll"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     
                                     
                            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                 picker.delegate = self;
                                                picker.allowsEditing = YES;
                                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                     
                                            [self presentViewController:picker animated:YES completion:NULL];
                                     //[alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];
    UIAlertAction* takeaphoto = [UIAlertAction
                                 actionWithTitle:@"Take a photo"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     
                                     UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                            picker.delegate = self;
                                            picker.allowsEditing = YES;
                                           picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                     
                                   [self presentViewController:picker animated:YES completion:NULL];
                                     
                                     
                                     //[alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];
    
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alertVC dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    
    [alertVC addAction:cameraRoll];
    [alertVC addAction:takeaphoto];
    [alertVC addAction:cancel];
    
    [alertVC.view setTintColor:[UIColor blackColor]];
    [self presentViewController:alertVC animated:YES completion:nil];

     //[self.navigationController popViewControllerAnimated:YES];
}
@end
