//
//  Screen5ViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 10/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ReceiptPhoto1ViewController.h"
#import "RegistrationViewController.h"
@interface ReceiptPhoto1ViewController ()

@end

@implementation ReceiptPhoto1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (void)selectPhoto {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
   // UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    //self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (IBAction)didTapBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];


}
- (IBAction)didTapYesPlease:(id)sender
{


 #pragma  mark : Action Sheet
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    UIAlertAction* cameraRoll = [UIAlertAction
                                    actionWithTitle:@"Camera Roll"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        [alertVC dismissViewControllerAnimated:YES completion:nil];
                                    }];
    UIAlertAction* takeaphoto = [UIAlertAction
                                     actionWithTitle:@"Take a photo"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         [alertVC dismissViewControllerAnimated:YES completion:nil];
                                     }];
    
    
    
    UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];

    
    [alertVC addAction:cameraRoll];
    [alertVC addAction:takeaphoto];
    [alertVC addAction:cancel];
    
    [alertVC.view setTintColor:[UIColor blackColor]];
    [self presentViewController:alertVC animated:YES completion:nil];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        
        
        //[self ];
//            
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = self;
//            picker.allowsEditing = YES;
//            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            
//            [self presentViewController:picker animated:YES completion:NULL];
//            
      
        
        // Set buttonIndex == 0 to handel "Ok"/"Yes" button response
        // Cancel button response
    }


else  if(buttonIndex == 1)
    
    {

        
        [self selectPhoto];
        
//        
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = YES;
//        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//        
//        [self presentViewController:picker animated:YES completion:NULL];



     }

}

- (IBAction)didTapNoThanks:(id)sender
{
    [self performSegueWithIdentifier:@"pushToScreen7" sender:self];
}
@end
