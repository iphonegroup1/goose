//
//  AppDelegate.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 08/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

@end

