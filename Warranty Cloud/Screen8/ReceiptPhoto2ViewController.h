//
//  Screen8ViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 10/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptPhoto2ViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *logoIconReceiptPg;

@property (weak, nonatomic) IBOutlet UIButton *didTapbackRecptPg;
@property (weak, nonatomic) IBOutlet UILabel *lblDoyouwant;
@property (weak, nonatomic) IBOutlet UIView *vwImgicon;
@property (weak, nonatomic) IBOutlet UIImageView *imgvwImgRecptPg;
@property (weak, nonatomic) IBOutlet UILabel *lbl1Vw2nd;
@property (weak, nonatomic) IBOutlet UILabel *lbl2Vw2nd;
@property (weak, nonatomic) IBOutlet UILabel *lbl3rdVw2nd;
@property (weak, nonatomic) IBOutlet UIView *Vw2nd;
@property (weak, nonatomic) IBOutlet UIView *Vw3rd;
- (IBAction)didTapYesPlease:(id)sender;
- (IBAction)didTapNoThanks:(id)sender;
- (IBAction)didTapBack:(id)sender;

@end
