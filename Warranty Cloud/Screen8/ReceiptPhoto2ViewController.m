//
//  Screen8ViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 10/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ReceiptPhoto2ViewController.h"

@interface ReceiptPhoto2ViewController ()

@end

@implementation ReceiptPhoto2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapYesPlease:(id)sender {
    
    
    
    
#pragma  mark : Action Sheet
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    UIAlertAction* cameraRoll = [UIAlertAction
                                 actionWithTitle:@"Camera Roll"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];
    UIAlertAction* takeaphoto = [UIAlertAction
                                 actionWithTitle:@"Take a photo"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [alertVC dismissViewControllerAnimated:YES completion:nil];
                                 }];
    
    
    
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 [alertVC dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    
    [alertVC addAction:cameraRoll];
    [alertVC addAction:takeaphoto];
    [alertVC addAction:cancel];
    
    [alertVC.view setTintColor:[UIColor blackColor]];
    [self presentViewController:alertVC animated:YES completion:nil];
    //[self performSegueWithIdentifier:@"pushToReceiptPhotoVC" sender:self];

}

- (IBAction)didTapNoThanks:(id)sender {
    [self performSegueWithIdentifier:@"pushToReceiptPhotoVC" sender:self];
}

- (IBAction)didTapBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
