//
//  AddNewViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 31/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ViewController.h"

@interface AddNewViewController : ViewController
- (IBAction)didTapBack:(id)sender;
@end
