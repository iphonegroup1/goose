//
//  AddNewViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 31/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "AddNewViewController.h"

@interface AddNewViewController ()

@end

@implementation AddNewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (IBAction)didTapBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
   // [self.navigationController popToViewController:<#(nonnull UIViewController *)#> animated:YES];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
