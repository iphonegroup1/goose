//
//  LoginViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 08/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UIAlertViewDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgVwicon;

@property (weak, nonatomic) IBOutlet UITextField *txtEmailAdd;

@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwRememberme;

@property (weak, nonatomic) IBOutlet UILabel *lblRememberMe;

@property (weak, nonatomic) IBOutlet UIImageView *imgVwEmail;
@property (weak, nonatomic) IBOutlet UIImageView *ImgVwPassword;
@property(weak,nonatomic)IBOutlet UIButton *btn_forgorPass;

@property (nonatomic, strong) UIPopoverController *userDataPopover; // Forgot Password Popup

@property(nonatomic,weak)IBOutlet UIView *ForgotPasswrd_Vw;
@property(nonatomic,weak)IBOutlet UIImageView *img_LoginBG;


#pragma Outlets of ForgotPasswrd_Vw
@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;
@property (weak, nonatomic) IBOutlet UILabel   *lblforgot_passwrd;
@property (weak, nonatomic) IBOutlet UILabel   *lblEnteremailadd;
@property(weak,nonatomic)IBOutlet UIButton *btn_FP_Submit;


@property(nonatomic,weak)IBOutlet UIView *loginVC_vw;



@property(nonatomic,weak)IBOutlet UIView *Back_vw;





- (IBAction)didTapContactCustomerCareService:(id)sender;


- (IBAction)didTapSubmit:(id)sender;







- (IBAction)didTapLogin:(id)sender;

- (IBAction)didTapForgotPassword:(id)sender;
- (IBAction)didTapNewUser:(id)sender;

@end
