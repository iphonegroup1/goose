//
//  RegistrationViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 08/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *ImgVwicon;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailId;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPass;


@property (weak, nonatomic) IBOutlet UIImageView *imgVwFrstname;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwLastName;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwEmailaddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwPass;
@property (weak, nonatomic) IBOutlet UIImageView *imgVwConfirmPass;


- (IBAction)didTapSignUp:(id)sender;

- (IBAction)didTapRUserLogin:(id)sender;



@end
