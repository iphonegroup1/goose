//
//  RegistrationViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 08/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "RegistrationViewController.h"

@interface RegistrationViewController ()

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtEmailId.userInteractionEnabled=NO;
    _txtFirstName.userInteractionEnabled=NO;
    _txtPassword.userInteractionEnabled=NO;
    _txtConfirmPass.userInteractionEnabled=NO;
    
    _txtLastName.userInteractionEnabled=NO;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapSignUp:(id)sender {
   
   [self performSegueWithIdentifier:@"SignUp" sender:self];
    
    
    
#pragma Validation checking:
    if(_txtFirstName.text.length<=0 || _txtLastName.text.length<=0 || _txtEmailId.text.length<=0 || _txtPassword.text.length<=0 || _txtConfirmPass.text.length<=0)
        
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Fill All Required Fields..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [_txtFirstName becomeFirstResponder];
        
        
    }
 
#pragma Email validation Checking through Catagory Class--------
     /* else if (![NSString validateEmail:[_txtEmailId text]])
      {
          UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter Proper Email Id..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
          [alert show];
          _txtEmailId.text=@"";
          [_txtEmailId becomeFirstResponder];
      
      
      }*/
    
#pragma Password validation Checking through Catagory Class--------
    else if (_txtPassword.text.length<6)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Password Must Contain Six Characters..." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        _txtPassword.text=@"";
        
        [_txtPassword becomeFirstResponder];
    }
    
    else if (![_txtPassword.text isEqualToString: _txtConfirmPass.text])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error" message:@"Password And Confirm Password Are Not Same...." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        _txtConfirmPass.text=@"";
        [_txtConfirmPass becomeFirstResponder];
        [alert show];
    }
    

   
    
}

- (IBAction)didTapRUserLogin:(id)sender {
    
    
   
    
//    [self performSegueWithIdentifier:@"RegUserLogin" sender:self];

  [self.navigationController popViewControllerAnimated:YES];
    
    
    
}
@end
