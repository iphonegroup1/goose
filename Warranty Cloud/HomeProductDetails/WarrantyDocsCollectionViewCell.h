//
//  WarrantyDocsCollectionViewCell.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 04/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WarrantyDocsCollectionViewCell : UICollectionViewCell

@property(strong,nonatomic)IBOutlet UIImageView *imgWarrantyDocs;


@end
