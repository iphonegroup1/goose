//
//  ProductDetailsViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 04/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import "ViewController.h"

@interface ProductDetailsViewController : ViewController
@property (weak, nonatomic) IBOutlet UILabel *lblTimeleft;



@property (weak, nonatomic) IBOutlet UILabel *lblShwTImeleft;
@property (weak, nonatomic) IBOutlet UITextField *txtItemName;
@property (weak, nonatomic) IBOutlet UITextField *txtStoreName;

@property(strong,nonatomic)NSArray *arrImgItem;
@property(strong,nonatomic)NSArray *arrImgWarrantyDocs;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollPItem;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollWDocs;



#pragma UIScrollView declarations






//@property(weak,nonatomic)IBOutlet   UICollectionView *collPhotosItem;
//@property(weak,nonatomic)IBOutlet      UICollectionView *collPhotosWarrantyDocs;


- (IBAction)didTapback2TabbarSearch1:(id)sender;

-(IBAction)didTapNext:(id)sender;




@end
