//
//  ProductDetailsViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 04/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import "ProductDetailsViewController.h"
#import "ItemCollectionViewCell.h"
#import "WarrantyDocsCollectionViewCell.h"


@interface ProductDetailsViewController ()

@end

@implementation ProductDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _txtItemName.userInteractionEnabled=NO;
    _txtStoreName.userInteractionEnabled=NO;
    
    
    _arrImgItem=[[NSArray alloc]initWithObjects:@"image_01.png",@"image_02.png",@"image_03.png",@"image_04.png", nil];
    
    _arrImgWarrantyDocs=[[NSArray alloc]initWithObjects:@"image_05.png",@"image_06.png",@"image_07.png",@"image_08.png", nil];
    
    
    
  //  [self.collPhotosItem registerNib:[UINib nibWithNibName:@"ItemCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellItem"];
    
    
    //[self.collPhotosWarrantyDocs registerNib:[UINib nibWithNibName:@"WarrantyDocsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"cellWarranDocs"];
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

/*
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return 1;
}*/

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _arrImgItem.count;
    
}
/*- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if(collectionView.tag==111)
    {
        ItemCollectionViewCell *cell1 = (ItemCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"celllist" forIndexPath:indexPath];
        
        cell1.imgItems.image=[UIImage imageNamed:[_arrImgItem objectAtIndex:indexPath.row]];
        //cell.lbllist.text=[NSString stringWithFormat:@"%@, %@",[NSString stringWithFormat:@"IMAGE%ld", (long)indexPath.row+1],[NSString stringWithFormat:@"%@",[_imgname objectAtIndex:indexPath.row]]];
        
           cell1.imgItems.layer.borderColor=[UIColor redColor].CGColor;
            cell1.imgItems.layer.borderWidth=2.0f;
           cell1.imgItems.layer.cornerRadius=1.0f;
      
        
        
        return cell1;
    }
     else(collectionView.tag==222);
      
    
    {
        
        
        WarrantyDocsCollectionViewCell *cell =(WarrantyDocsCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"cellWarranDocs" forIndexPath:indexPath];
        
        cell.imgWarrantyDocs.image=[UIImage imageNamed:[_arrImgWarrantyDocs objectAtIndex:indexPath.row]];
        
        
        cell.imgWarrantyDocs.layer.borderColor=[UIColor redColor].CGColor;
        cell.imgWarrantyDocs.layer.borderWidth=2.0f;
        cell.imgWarrantyDocs.layer.cornerRadius=1.0f;
       // cell.txtlbl.text=[NSString stringWithFormat:@"%@, %@",[NSString stringWithFormat:@"IMAGE%ld", (long)indexPath.row+1],[NSString stringWithFormat:@"%@",[_imgname objectAtIndex:indexPath.row]]];
        
        
        //     cell.t.layer.borderColor=[UIColor blueColor].CGColor;
        //     cell.lbllist.layer.borderWidth=2.0f;
        //     cell.lbllist.text=[NSString stringWithFormat:@"%@, %@",[NSString stringWithFormat:@"PLACES%ld", (long)indexPath.row+1],[NSString stringWithFormat:@"%@",[_listname objectAtIndex:indexPath.row]]];
        // Configure cell with data
//        UIImageView *myImageView = (UIImageView *)[cell viewWithTag:100];
//        myImageView.image = [UIImage imageNamed:[itemsArray objectAtIndex:indexPath.row]];
//        
        return cell;
    }
}*/
/*
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    float width;
    if(_collPhotosItem.tag==111)
    {
        width=self.collPhotosItem.frame.size.width;
        return CGSizeMake(width, 80);
    }
    else
    {
        width=(collectionView.frame.size.width/2)-10;
        return CGSizeMake(width, width);
    }
    
    
}*/






//Push2PDetails

//[self performSegueWithIdentifier:@"push2ProDetails" sender:self];

-(IBAction)didTapNext:(id)sender
{
    
    
    [self performSegueWithIdentifier:@"Push2PDetails" sender:self];
    
    
}



- (void)didReceiveMemoryWarning
    
{

    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapback2TabbarSearch1:(id)sender {
    
    
    //pushToProductdetails
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
@end
