//
//  FilterCategoryDetailsViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 21/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import "FilterCategoryDetailsViewController.h"

@interface FilterCategoryDetailsViewController ()

@end

@implementation FilterCategoryDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
}






- (IBAction)didTapBack:(id)sender
{
    
   
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
    
}

-(IBAction)didTapElectronics:(id)sender
{
    
    
    [self performSegueWithIdentifier:@"Go2CategoryDetailsII" sender:self];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
