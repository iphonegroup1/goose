//
//  FilterCategoryDetailsViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 21/01/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCategoryDetailsViewController : UIViewController



-(IBAction)didTapElectronics:(id)sender;



- (IBAction)didTapBack:(id)sender;
@end
