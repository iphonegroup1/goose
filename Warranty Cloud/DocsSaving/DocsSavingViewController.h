//
//  DocsSavingViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 17/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocsSavingViewController : UIViewController
- (IBAction)didTapBack:(id)sender;
- (IBAction)didTapLetsGo:(id)sender;


@property(weak,nonatomic)IBOutlet UITextField *txtemailadd;
@property(weak,nonatomic)IBOutlet UITextField *txtpassword;


@end
