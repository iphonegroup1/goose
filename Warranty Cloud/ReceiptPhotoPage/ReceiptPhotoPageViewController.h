//
//  ReceiptPhotoPageViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 11/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptPhotoPageViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *imgOnTop;
- (IBAction)didTapBck:(id)sender;

- (IBAction)didTapSaveandTakeAnother:(id)sender;
- (IBAction)didTapSaveandContinue:(id)sender;
- (IBAction)didTapTakePhotoAgain:(id)sender;


@end
