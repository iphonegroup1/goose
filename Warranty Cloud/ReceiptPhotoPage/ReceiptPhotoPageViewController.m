//
//  ReceiptPhotoPageViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 11/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "ReceiptPhotoPageViewController.h"

@interface ReceiptPhotoPageViewController ()

@end

@implementation ReceiptPhotoPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapBck:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didTapSaveandTakeAnother:(id)sender {
}

- (IBAction)didTapSaveandContinue:(id)sender
{
    [self performSegueWithIdentifier:@"pushToAddProductDetails" sender:self];
}

- (IBAction)didTapTakePhotoAgain:(id)sender {
}
@end
