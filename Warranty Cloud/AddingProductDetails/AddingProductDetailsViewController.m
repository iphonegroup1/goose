//
//  AddingProductDetailsViewController.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 11/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import "AddingProductDetailsViewController.h"

#import "CustomBarButton.h"

#import "AppDelegate.h"

@interface AddingProductDetailsViewController ()

@end

@implementation AddingProductDetailsViewController
{
    
    
    
    
    CGPoint pointtrack,pointtrackcontentoffset;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _txtBrand.userInteractionEnabled=NO;
    _txtCategory.userInteractionEnabled=NO;
    _txtProNAme.userInteractionEnabled=NO;
    _txtPurchase_date.userInteractionEnabled=NO;
    _txtPurchasePrice.userInteractionEnabled=NO;
    _txtRemindMe.userInteractionEnabled=NO;
    _txtSerialNumber.userInteractionEnabled=NO;
    _txtStoreAdd.userInteractionEnabled=NO;
    _txtStoreName.userInteractionEnabled=NO;
    _txtWarrantyPeriod.userInteractionEnabled=NO;
    //_txtWarrExpDate.userInteractionEnabled=NO;
    
    
 // [_scrollReg setContentSize:CGSizeMake(0, _scrollReg.frame.size.height+20)];
    
    //  [_scrollReg setContentSize:CGSizeMake(_scrollReg.frame.size.width, _scrollReg.frame.size.height+20)];


 //   [_scrollReg setContentSize:CGSizeMake(320, 678)];
    
    //[_scrollReg setClipsToBounds:YES];
    
    // Calculate scroll view size
   /* float sizeOfContent = 0;
    int i;
    for (i = 0; i < [_scrollReg.subviews count]; i++)
    {
        
        UIView *view =[_scrollReg.subviews objectAtIndex:i];
        sizeOfContent += view.frame.size.height;
    }*/
    
    // Set content size for scroll view
     [_scrollReg setContentSize:CGSizeMake(_scrollReg.bounds.size.width, _scrollReg.bounds.size.height*1.2)];

  //  _scrollReg.contentSize = CGSizeMake(_scrollReg.bounds.size.width, sizeOfContent+10);
    
    //_scrollReg.contentSize = CGSizeMake(_scrollReg.frame.size.width, sizeOfContent+10);
    
    
    // Do any additional setup after loading the view.
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.view viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else
    {
        // Not found, so remove keyboard.
        //[self dismisskeyboard];
    }
    [textField resignFirstResponder];
    // Setting Scroll View after textfield returns
    [_scrollReg setContentOffset:rt animated:YES];
    return YES;
    

}


// Setting scroll when user starts edditing a textfield.
#pragma textfield delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
   // txtfld.inputAccessoryView=keyBoardToolBar;

    CGRect rc=[textField bounds];
    rc=[textField convertRect:rc toView:_scrollReg];
    CGPoint pt;
    pt=rc.origin;
    
    pt.x=0;
    pt.y-=60;
    [_scrollReg setContentOffset:pt animated:YES];

    
    [self.scrollReg setContentOffset:
     CGPointMake(0, -self.scrollReg.contentInset.top) animated:YES];

    //
    
    //self.activeTextField=textField;
//    self.txtWarrantyPeriod=textField;
//    
//    
//    pointtrack = textField.superview.frame.origin;
//    
//    CGRect rect=self.view.frame;
//    rect.origin=pointtrackcontentoffset;
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.3];
//    if(SCREEN_HEIGHT==480||SCREEN_HEIGHT==568)
//    {
//        if(pointtrack.y>120)
//        {
//            if(pointtrack.y>350)
//            {
//                pointtrack.y=340;
//            }
//            rect.origin.y-=pointtrack.y-130;
//        }
//    }
//    
//    else if(SCREEN_HEIGHT==667)
//    {
//        if(pointtrack.y>150.0)
//        {
//            rect.origin.y-=pointtrack.y-100;
//        }
//    }
//    else if(SCREEN_HEIGHT==736)
//    {
//        if(pointtrack.y>150.0)
//        {
//            rect.origin.y-=pointtrack.y-120;
//        };
//    }
//    else
//    {
//        
//    }
//    self.view.frame=rect;
//    [UIView commitAnimations];
    
    
    
    
    
    
    
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)didTapNext:(id)sender {
    
    //DocsSaving
    
  [self performSegueWithIdentifier:@"DocsSaving" sender:self];
    
    
}
- (IBAction)didTapCategory:(id)sender
{
    
    
}
- (IBAction)didTapBrand:(id)sender
{
    
    
}
- (IBAction)didTapPurchaseDate:(id)sender
{
    
    
    
}
- (IBAction)didTapWarrantyPeriod:(id)sender
{
    
    
    
}




#pragma WARRANTY EXPIRY DATE:

- (IBAction)didTapWarrantyExpDate:(id)sender
{
#pragma Date picker :Format- Month,Date,Year
    //Date Picker
//    
//    NSLog(@"tag is.............3");
//    
//    _pickerDate=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, -50, 320, 300)];
//    
//    
//    _pickerDate.datePickerMode=UIDatePickerModeDate;
//    [_pickerDate setMinimumDate:[NSDate date]];
//    
//    _pickerDate.backgroundColor=[UIColor whiteColor];
//    [_pickerDate setMultipleTouchEnabled:YES];
//    
//    
//    
//    [_Botton_Vw addSubview:_pickerDate]; //to add Datepicker
//    [_Botton_Vw setMultipleTouchEnabled:NO];
//    _pickerDate.hidden = NO;
//    _pickerDate.date = [NSDate date];
//    _pickerDate.timeZone = [NSTimeZone localTimeZone];
//    
//    [_pickerDate addTarget:self action:@selector(pickerOFF) forControlEvents:UIControlEventValueChanged];
//    
//    
//    format=[[NSDateFormatter alloc]init];
//    
//    
//    
//    [format setDateFormat:@"mm-dd-YYYY"];
//    
//    
//    
//     NSString *stringFromDate = [format stringFromDate:self.pickerDate.date];
//    
//    
//    
//     [_txtWarrExpDate addSubview:_pickerDate];
//    
//    
//    NSMutableArray *barItems = [[NSMutableArray alloc] init];
//    
//     // [_mypickerToolbar setItems:barItems animated:YES];
//   //_txtWarrExpDate.inputAccessoryView = _mypickerToolbar;
//    
//    
//    
//#pragma toolbar item done and cancel
//    
//   // [_Botton_Vw addSubview:_pickerDate];
//    UIToolbar* toolbar = [[UIToolbar alloc] init];
//    toolbar.frame=CGRectMake(-10,0,320,40);
//    
//    //toolbar.barStyle = uib;
//    
//    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    [_pickerDate addSubview:toolbar];
//    
//
//    _txtWarrantyPeriod.inputAccessoryView = toolbar;
//    
//    
    
    NSLog(@"tag is.............3");
    _pickerDate=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, -50, 320, 300)];
    
    _pickerDate.datePickerMode=UIDatePickerModeDate;
    [_pickerDate setMinimumDate:[NSDate date]];
    _pickerDate.backgroundColor=[UIColor whiteColor];
    _pickerDate.hidden = NO;
    _pickerDate.date = [NSDate date];
    _pickerDate.timeZone = [NSTimeZone localTimeZone];
    [_pickerDate addTarget:self action:@selector(Donedatepicker) forControlEvents:UIControlEventValueChanged];
    format=[[NSDateFormatter alloc]init];
    [format setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [_Botton_Vw addSubview:_pickerDate];
    
    _mypickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
    _mypickerToolbar.barStyle = UIBarButtonItemStyleDone;
    [_mypickerToolbar sizeToFit];
    
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    [_pickerDate addSubview:_mypickerToolbar];
    
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(pickerDoneClicked)];
    [barItems addObject:doneBtn];
    
    [_mypickerToolbar setItems:barItems animated:YES];
    
    _txtWarrExpDate.inputAccessoryView = _mypickerToolbar;

    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                     style:UIBarButtonSystemItemDone target:self
    
                                                                  action:@selector(Donedatepicker)];
    [barItems addObject:doneButton];
    
    [_mypickerToolbar setItems:barItems animated:YES];
    
    _txtWarrExpDate.inputAccessoryView = _mypickerToolbar;
    
 
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [_pickerDate addSubview:_mypickerToolbar];
    
    [_mypickerToolbar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft, doneButton, nil]];

    
    

    
#pragma Backward or previous button
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"arrow_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [_mypickerToolbar addSubview:custombutton];
    [custombutton addSubview:buttonimageview];
    
#pragma Forward or front button:
    UIButton *custombuttonForward=[[UIButton alloc]initWithFrame:CGRectMake(40, 0, 30, 30)];
     [custombuttonForward addTarget:self action:@selector(nextDateafter2) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview2=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview2.image=[UIImage imageNamed:@"arrow_iconForward.png"];
    buttonimageview2.contentMode=UIViewContentModeScaleAspectFit;
    [_mypickerToolbar addSubview:custombuttonForward];
    [custombuttonForward addSubview:buttonimageview2];
    
    
}
-(void)back
{
    NSLog(@"dasasddsadsa");
 
    
}

-(IBAction)nextDateafter2:(id)sender
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Notification"
                                          message:@"You have 55555 a date"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    
    [alertController addAction:okAction];
    
    
    [self presentViewController:alertController animated:YES completion:nil];

    
    
}
-(void)nextDateafter2
{
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Notification"
                                          message:@"You have selected a date"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    
    [alertController addAction:okAction];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)pickerOFF
{
    NSString *dateStri=[format stringFromDate:_pickerDate.date];
    
    [_txtWarrExpDate setText:[NSString stringWithFormat:@"%@",dateStri]];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[format stringFromDate:_pickerDate.date]]);
    
    
    
}


-(void)Donedatepicker
{
    [_pickerDate removeFromSuperview];

    
}



//-(IBAction)didTapPicker:(id)sender
//{
//    [self pickerDoneClicked];
//    
//    
//}



/*-(void)Donedatepicker
{
    // NSDateFormatter *format=[[NSDateFormatter alloc] init];
    NSString *dateStri=[format stringFromDate:_pickerDate.date];
    
    [_txtWarrExpDate setText:[NSString stringWithFormat:@"%@",dateStri]];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[format stringFromDate:_pickerDate.date]]);
    
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Notification"
                                          message:@"You have selected a date"
                                          preferredStyle:UIAlertControllerStyleAlert];
    
 
   UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   NSLog(@"OK action");
                               }];
    
    
    //[alertController addAction:okAction];
    
    
    //[self presentViewController:alertController animated:YES completion:nil];
    
    
    
    //[_pickerDate removeFromSuperview];

    
    
  
    
    
    
    
}*/


- (IBAction)didTapBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



@end
