//
//  CustomBarButton.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 03/02/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomBarButton : UIBarButtonItem


+(UIButton *)setbarbutton;

@end
