//
//  CustomBarButton.m
//  Warranty Cloud
//
//  Created by SampritaRoy on 03/02/16.
//  Copyright © 2016 SampritaRoy. All rights reserved.
//

#import "CustomBarButton.h"

@implementation CustomBarButton





+(UIButton *)setbarbutton
{
    UIButton *custombutton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    // [custombutton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    custombutton.backgroundColor=[UIColor clearColor];
    UIImageView *buttonimageview=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 20, 20)];
    buttonimageview.image=[UIImage imageNamed:@"arrow_icon.png"];
    buttonimageview.contentMode=UIViewContentModeScaleAspectFit;
    [custombutton addSubview:buttonimageview];
    return custombutton;
}



@end
