//
//  AddingProductDetailsViewController.h
//  Warranty Cloud
//
//  Created by SampritaRoy on 11/12/15.
//  Copyright © 2015 SampritaRoy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddingProductDetailsViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    CGPoint rt;
    NSDateFormatter *format;

    
}


- (IBAction)didTapBack:(id)sender;

@property(strong,nonatomic)IBOutlet UIScrollView *scrollReg;

@property (weak, nonatomic) IBOutlet UITextField *txtProNAme;
@property (weak, nonatomic) IBOutlet UITextField *txtStoreName;

@property (weak, nonatomic) IBOutlet UITextField *txtStoreAdd;
@property (weak, nonatomic) IBOutlet UITextField *txtCategory;
@property (weak, nonatomic) IBOutlet UITextField *txtBrand;
@property (weak, nonatomic) IBOutlet UITextField *txtPurchase_date;
@property (weak, nonatomic) IBOutlet UITextField *txtWarrantyPeriod;
@property (weak, nonatomic) IBOutlet UITextField *txtWarrExpDate;
@property (weak, nonatomic) IBOutlet UITextField *txtPurchasePrice;
@property (weak, nonatomic) IBOutlet UITextField *txtSerialNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtRemindMe;



@property(strong,nonatomic)IBOutlet UIToolbar *mypickerToolbar;


@property(strong,nonatomic)IBOutlet UIDatePicker *pickerDate;


@property (strong, nonatomic) IBOutlet UIView *Botton_Vw;





- (IBAction)didTapNext:(id)sender;

- (IBAction)didTapCategory:(id)sender;

- (IBAction)didTapBrand:(id)sender;
- (IBAction)didTapPurchaseDate:(id)sender;
- (IBAction)didTapWarrantyPeriod:(id)sender;

- (IBAction)didTapWarrantyExpDate:(id)sender;
-(IBAction)didTapPicker:(id)sender;

#pragma  date picker:
-(IBAction)didtapPrevious:(id)sender;

-(IBAction)didtapNextDate:(id)sender;

-(UIButton *)setbarbutton;





@end
